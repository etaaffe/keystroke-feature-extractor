'''
Created on Mar 15, 2013

@author: vinnie
'''

import numpy as np
import pandas as pd
from IPython import embed
from pprint import pprint
from sklearn import manifold
from itertools import product, combinations
from functools import partial
from scipy.stats import linregress
from collections import defaultdict, Counter
from scipy.spatial.distance import cdist, pdist, squareform

from features import FEATURES, FALLBACKS
from keycode import *

FALLBACK_WEIGHT = 0.25 # weight of fallback observations
T_MIN_FREQUENCY = 3 # min frequency per sample for computing correlation
M_MIN_FREQUENCY = 5 # min frequency per sample for feature fallback

def split_data(df, query_size):
    
    s = pd.DataFrame.from_records(list(df.index.unique())).groupby(0).apply(lambda x: x[1].values)
    s.apply(lambda x: np.random.shuffle(x))
    
    r = s.apply(lambda x: x[query_size:])
    q = s.apply(lambda x: x[:query_size])
    
    reference = pd.concat([df.loc[(u,s)] for u,row in r.iteritems() for s in row])
    query = pd.concat([df.loc[(u,s)] for u,row in q.iteritems() for s in row])
    
    return reference, query


def transition_digrams(df, distance=1):
    a = df.groupby(['user','session']).apply(lambda x: x[:-distance].reset_index())
    b = df.groupby(['user','session']).apply(lambda x: x[distance:].reset_index())
    
    a = a[['user','session','keyname','timepress','timerelease']]
    b = b[['keyname','timepress','timerelease']]
    
    a.columns = ['user','session','keyname_1', 'timepress_1', 'timerelease_1']
    b.columns = ['keyname_2', 'timepress_2', 'timerelease_2']
    
    joined = pd.concat([a,b], join='inner', axis=1)
    
    cols=['user','session','keynames','transition']
    
    # Create columns for each transition type
    t1 = pd.DataFrame({'user': joined['user'], 
                       'session': joined['session'],
                       'keynames': joined['keyname_1'] + '__' + joined['keyname_2'],
                       'transition': joined['timepress_2'] - joined['timerelease_1']},
                      columns=cols, index=joined.index)
    
    t2 = pd.DataFrame({'user': joined['user'], 
                       'session': joined['session'],
                       'keynames': joined['keyname_1'] + '__' + joined['keyname_2'],
                       'transition': joined['timepress_2'] - joined['timepress_1']},
                      columns=cols, index=joined.index)
    
    return t1, t2

def outlier_removal_recursive(df, col, group, std_distance=4, max_iterations=-1):
    '''
    Remove duration outliers on a per-user basis
    
    10 iterations will remove most outliers.
    
    Does the following:
        group df by user and keyname
        get mean and std for each group (user/keyname combination)
        filter df durations with the corresponding user/key mean and stds
    
    This could be more efficient by testing the number of outliers removed for 
    each group and only recomputing the groups with more than 0 removed
    '''
    prev_len = np.inf
    i = 0
    while prev_len > len(df):
        prev_len = len(df)
        df = outlier_removal(df, df, col, group, std_distance=std_distance)
        
        i += 1
        if max_iterations > 0 and i == max_iterations:
            break
        
    return df

def outlier_removal(dfmeta, df, col, group, std_distance=4):
    '''
    Remove duration outliers on a per-user basis
    
    10 iterations will remove most outliers.
    
    Does the following:
        group df by user and keyname
        get mean and std for each group (user/keyname combination)
        filter df durations with the corresponding user/key mean and stds
    
    This could be more efficient by testing the number of outliers removed for 
    each group and only recomputing the groups with more than 0 removed
    '''
    m = dfmeta[col].groupby(dfmeta[group]).mean().to_dict()
    s = dfmeta[col].groupby(dfmeta[group]).std().fillna(0).to_dict()
    
    means = df[group].map(m).values
    stds = df[group].map(s).values
    
    lower = means - std_distance*stds
    upper = means + std_distance*stds
    
    prev_len = len(df)
    df = df.loc[(df[col].values > lower) & (df[col].values < upper)]
    print('Removed %d observations' %(prev_len - len(df)))
    
    return df

def reverse_tree(features, hierarchy):
    parents = {}
    
    for parent,children in hierarchy.items():
        for child in children:
            parents[child] = parent
    
    return parents

def normalize(df, method, distance=2):
    
    if method == 'stddev':
        for col in df.columns - ['user', 'session']:
            upper = df[col].mean() + distance*df[col].std()
            lower = df[col].mean() - distance*df[col].std()
            df[col] = (df[col] - lower)/(upper - lower)
            df[col][df[col] < 0] = 0
            df[col][df[col] > 1] = 1
    elif method == 'minmax':
        for col in df.columns - ['user', 'session']:
            df[col] = (df[col] - df[col].min())/(df[col].max() - df[col].min())
    else:
        print('Invalid normalization method')
        return None
        
    return df

def normalize_meta(dfmeta, df, method, distance=2):
    
    if method == 'stddev':
        for col in df.columns - ['user', 'session']:
            upper = (dfmeta[col].mean() + distance*dfmeta[col].std()).copy()
            lower = (dfmeta[col].mean() - distance*dfmeta[col].std()).copy()
            
            dfmeta[col] = (dfmeta[col] - lower)/(upper - lower)
            dfmeta[col][dfmeta[col] < 0] = 0
            dfmeta[col][dfmeta[col] > 1] = 1
            
            df[col] = (df[col] - lower)/(upper - lower)
            df[col][df[col] < 0] = 0
            df[col][df[col] > 1] = 1
    elif method == 'minmax':
        for col in df.columns - ['user', 'session']:
            
            upper = dfmeta[col].max().copy()
            lower = dfmeta[col].min().copy()
            
            dfmeta[col] = (dfmeta[col] - lower)/(upper - lower)
            dfmeta[col][dfmeta[col] < 0] = 0
            dfmeta[col][dfmeta[col] > 1] = 1
            
            df[col] = (df[col] - lower)/(upper - lower)
            df[col][df[col] < 0] = 0
            df[col][df[col] > 1] = 1
    else:
        print('Invalid normalization method')
        return None
        
    return dfmeta, df

def extract_gaussian_features(df, group_col_name, feature_col_name, features, decisions, feature_name_prefix):
     
    feature_vector = {}
    for feature_name,feature_set in features.items():
        full_feature_name = '%s%s' %(feature_name_prefix, feature_name)
         
        obs = df.loc[df[group_col_name].isin(feature_set), feature_col_name]
         
        if len(obs) < M_MIN_FREQUENCY and feature_name in decisions.keys():
            fallback_name =  decisions[feature_name]
            fallback_obs = pd.DataFrame()
            while len(obs) + len(fallback_obs) < M_MIN_FREQUENCY:
                fallback_set = globals()[fallback_name]
                fallback_obs = df.loc[df[group_col_name].isin(fallback_set), feature_col_name]
                 
                if fallback_name in decisions.keys():
                    fallback_name = decisions[fallback_name] # go up the tree
                else:
                    break # reached the root node
             
            n = len(obs)
             
            # Prevent NA values
            if n == 0:
                obs_mean = 0
                obs_std = 0
            elif n == 1:
                obs_mean = obs.mean()
                obs_std = 0
            else:
                obs_mean = obs.mean()
                obs_std = obs.std()
             
            feature_vector['%s.mean' %full_feature_name] = (n*obs_mean + FALLBACK_WEIGHT*fallback_obs.mean())/(n + FALLBACK_WEIGHT)
            feature_vector['%s.std' %full_feature_name] = (n*obs_std + FALLBACK_WEIGHT*fallback_obs.std())/(n + FALLBACK_WEIGHT)
        else:
            feature_vector['%s.mean' %full_feature_name] = obs.mean()
            feature_vector['%s.std' %full_feature_name] = obs.std()
     
    return pd.Series(feature_vector)

def extract_histogram_features(df, group_col_name, feature_col_name, features, decisions, bins):
    
    feature_vector = []
    for feature_name,feature_set in features.items():
        obs = df.loc[df[group_col_name].isin(feature_set), feature_col_name]
        
        if len(obs) < M_MIN_FREQUENCY and feature_name in decisions.keys():
            fallback_name =  decisions[feature_name]
            while len(obs) < M_MIN_FREQUENCY:
                fallback_set = globals()[fallback_name]
                obs = df.loc[df[group_col_name].isin(fallback_set), feature_col_name]
                
                if fallback_name in decisions.keys():
                    fallback_name = decisions[fallback_name] # go up the tree
                else:
                    break # reached the root node
        
        features = np.histogram(obs.values, bins, density=True)[0]
        feature_vector.extend(features)
    
    return pd.Series(feature_vector)

def durations(df):
    
    return pd.DataFrame({'keyname':df['keyname'].values, 'duration':df['timerelease'].values - df['timepress'].values})

def transitions(df):
    
    keynames = df[:-1]['keyname'].values + '__' + df[1:]['keyname'].values
    t1 = df[1:]['timepress'].values - df[:-1]['timerelease'].values
    t2 = df['timepress'].diff().dropna().values
    t3 = df['timerelease'].diff().dropna().values
    t4 = df[1:]['timerelease'].values - df[:-1]['timepress'].values
    
    return pd.DataFrame({'keynames':keynames,'t1':t1,'t2':t2,'t3':t3,'t4':t4})

def clean_features(df):
    
    df[(df==np.inf)|(df==-np.inf)|(np.isnan(df))] = 0
    return df

def estimate_bins(x, n, outlier=2):
    """
    Using Scotts rule
    """
    x = x[(x!=np.inf)&(x!=-np.inf)&(np.isnan(x)==False)]
    
    sigma = x.std()
    mean = x.mean()
    x = x[(x>(mean-outlier*sigma))&(x<(mean+outlier*sigma))]
    
    h = (3.5*x.std())/np.power(n, 1.0/3)
    k = (x.max() - x.min())/h

    return np.linspace(x.min(), x.max(), k+1)

def extract_keystroke_features(df, levels, featureset, fallback, measure):
    
    features = FEATURES[featureset]
    fallback = FALLBACKS[fallback]
    
    duration_features = {k:v for k,v in features.items() if '__' not in k}
    transition_features = {k:v for k,v in features.items() if '__' in k}
    decisions = reverse_tree(features, fallback)
    
    du_features = pd.DataFrame()
    if len(duration_features) > 0:
        d = df.groupby(level=levels).apply(durations)
        du = outlier_removal_recursive(d, 'duration', 'keyname')
        
        if measure == 'histogram':
            du_bins = estimate_bins(du['duration'].values, du.groupby(level=levels).size().mean())
            du_features = du.groupby(level=levels).apply(lambda x:
                                                extract_histogram_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    bins=du_bins))
        else:
            du_features = du.groupby(level=levels).apply(lambda x:
                                                extract_gaussian_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    feature_name_prefix='du_'))
    
    t1_features, t2_features = pd.DataFrame(), pd.DataFrame()
    if len(transition_features) > 0:
        t = df.groupby(level=levels).apply(transitions)
        t1 = outlier_removal_recursive(t[['keynames','t1']], 't1', 'keynames')
        t2 = outlier_removal_recursive(t[['keynames','t2']], 't2', 'keynames')
        
        if measure == 'histogram':
            t1_bins = estimate_bins(t1['t1'].values, t1.groupby(level=levels).size().mean())
            t2_bins = estimate_bins(t2['t2'].values, t2.groupby(level=levels).size().mean())
            t1_features = t1.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t1',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t1_bins))
            t2_features = t2.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t2',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t2_bins))
        else:
            t1_features = t1.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t1',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t1_'))
            t2_features = t2.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t2',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t2_'))
    
    fspace = pd.concat([du_features, t1_features, t2_features], axis=1)
    fspace = clean_features(fspace)
    
    return fspace

def extract_keystroke_features_meta(dfmeta, df, levels, featureset, fallback, measure):
    
    features = FEATURES[featureset]
    fallback = FALLBACKS[fallback]
    
    duration_features = {k:v for k,v in features.items() if '__' not in k}
    transition_features = {k:v for k,v in features.items() if '__' in k}
    decisions = reverse_tree(features, fallback)
    
    du_features = pd.DataFrame()
    if len(duration_features) > 0:
        dmeta = dfmeta.groupby(level=levels).apply(durations)
        d = df.groupby(level=levels).apply(durations)
        dumeta = outlier_removal_recursive(dmeta, 'duration', 'keyname')
        du = outlier_removal(dmeta, d, 'duration', 'keyname')
        
        if measure == 'histogram':
            du_bins = estimate_bins(dumeta['duration'].values, dumeta.groupby(level=levels).size().mean())
            du_features = du.groupby(level=levels).apply(lambda x:
                                                extract_histogram_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    bins=du_bins))
            
            du_featuresmeta = dumeta.groupby(level=levels).apply(lambda x:
                                                extract_histogram_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    bins=du_bins))
        else:
            du_features = du.groupby(level=levels).apply(lambda x:
                                                extract_gaussian_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    feature_name_prefix='du_'))
            
            du_featuresmeta = dumeta.groupby(level=levels).apply(lambda x:
                                                extract_gaussian_features(x, feature_col_name='duration',
                                                                    group_col_name='keyname',
                                                                    features=duration_features,
                                                                    decisions=decisions,
                                                                    feature_name_prefix='du_'))
    
    t1_features, t2_features = pd.DataFrame(), pd.DataFrame()
    if len(transition_features) > 0:
        t = df.groupby(level=levels).apply(transitions)
        tmeta = dfmeta.groupby(level=levels).apply(transitions)
        t1meta = outlier_removal_recursive(tmeta[['keynames','t1']], 't1', 'keynames')
        t2meta = outlier_removal_recursive(tmeta[['keynames','t2']], 't2', 'keynames')
        t1 = outlier_removal(t1meta, t[['keynames','t1']], 't1', 'keynames')
        t2 = outlier_removal(t2meta, t[['keynames','t2']], 't2', 'keynames')
        
        if measure == 'histogram':
            t1_bins = estimate_bins(t1meta['t1'].values, t1meta.groupby(level=levels).size().mean())
            t2_bins = estimate_bins(t2meta['t2'].values, t2meta.groupby(level=levels).size().mean())
            t1_features = t1.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t1',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t1_bins))
            t2_features = t2.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t2',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t2_bins))
            
            t1_featuresmeta = t1meta.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t1',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t1_bins))
            t2_featuresmeta = t2meta.groupby(level=levels).apply(lambda x:
                                        extract_histogram_features(x, feature_col_name='t2',
                                                                    group_col_name='keynames',
                                                                    features=transition_features,
                                                                    decisions=decisions,
                                                                    bins=t2_bins))
        else:
            t1_features = t1.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t1',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t1_'))
            t2_features = t2.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t2',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t2_'))
            
            t1_featuresmeta = t1meta.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t1',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t1_'))
            t2_featuresmeta = t2meta.groupby(level=levels).apply(lambda x:
                                            extract_gaussian_features(x, feature_col_name='t2',
                                                                group_col_name='keynames',
                                                                features=transition_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='t2_'))
    
    
    fspace = pd.concat([du_features, t1_features, t2_features], axis=1)
    fspace = clean_features(fspace)
    
    fspacemeta = pd.concat([du_featuresmeta, t1_featuresmeta, t2_featuresmeta], axis=1)
    fspacemeta = clean_features(fspacemeta)
    
    return fspacemeta, fspace

from sklearn.feature_extraction import FeatureHasher

def unstack_events(df):
    
    presses = df[['timepress','keyname']]
    presses['keyname'] = '+' + presses['keyname']
    presses.columns = ['time','action']
    
    releases = df[['timerelease','keyname']]
    releases['keyname'] = '-' + releases['keyname']
    releases.columns = ['time','action']
    
    df = pd.concat([presses,releases]).sort(['time'])
    
    return df

def extract_keystroke_feature_hash(df, levels, num_features):
    
    hasher = FeatureHasher(input_type='pair', n_features=num_features)
    df = df.groupby(level=levels).apply(unstack_events).reset_index(level=[2,3], drop=True)
    data = [(idx, (('{0} {1}'.format(k1,k2), t2-t1) for (t1,k1),(t2,k2) in zip(k.values[:-1], k.values[1:]))) for idx,k in df.groupby(level=levels)]
    X = hasher.transform((k for _,k in data))
    fspace = pd.DataFrame(X.todense(), index=pd.MultiIndex.from_tuples([idx for idx,_ in data]))
    fspace.index.names = ['user','session']
    fspace = clean_features(fspace)
    return fspace