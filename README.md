### Keystroke feature extractor

## Usage

```
python main.py extract [-h] [--features FEATURES] [--measure MEASURE]
                       [--fallback FALLBACK] [--normalization NORMALIZATION]
                       [--norm_distance NORM_DISTANCE] [--groupby GROUPBY]
                       input output

positional arguments:
  input                 Raw keystroke data file
  output                Feature file output

optional arguments:
  -h, --help            show this help message and exit
  --features FEATURES   Feature set
  --measure MEASURE     Feature measure function
  --fallback FALLBACK   Fallback hierarchy
  --normalization NORMALIZATION
                        Normalization method
  --norm_distance NORM_DISTANCE
                        Std distance for std normalivation
  --groupby GROUPBY     Groupby columns
```