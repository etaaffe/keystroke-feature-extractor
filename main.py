"""
Starting point for the Keystroke feature extractor (KFE)
"""

def main():
    import sys
    from inspect import getmembers, isfunction
    from argparse import ArgumentParser
    import commands
    all_functions = dict((name, fn) for name, fn in getmembers(commands, isfunction) if '_'!=name[0])
    
    parser = ArgumentParser(prog="kfe")
    parser.add_argument('command')
    args = parser.parse_args(sys.argv[1:2])
    
    if args.command not in all_functions.keys():
        print('Unrecognized function:', args.command)
        print('Use one of these functions:')
        for c,f in sorted(all_functions.items()):
            print('kfe', c, f.__doc__)
        return
            
    all_functions[args.command](sys.argv[2:]) 
    
if __name__ == '__main__':
    main()