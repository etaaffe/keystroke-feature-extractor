'''
Created on May 15, 2013

@author: vinnie
'''

import os
import csv
import string
import numpy as np
from itertools import product
from collections import defaultdict, Counter

WORKING_DIR = '/home/vinnie/workspace/biometrics/src/bas/src/bas/fallback-models'

def load_correlation_model(decision_filename, ax_filename, b_filename):
    
    decision = {}
    fallback = {}
    
    r_decision = csv.reader(open(decision_filename, 'rt'))
    for row in r_decision:
        decision[row[0].lower()] = [a.lower() for a in row[1:]]
    
    r_ax = csv.reader(open(ax_filename, 'rt'))
    r_b = csv.reader(open(b_filename, 'rt'))
    
    rows_ax = [l for l in r_ax]
    rows_b = [l for l in r_b]
    
    a_keys = {k1: {k2:float(a) for k2,a in zip(string.ascii_lowercase, row)} 
               for k1,row in zip(string.ascii_lowercase, rows_ax)}
    
    b_keys = {k1: {k2:float(b) for k2,b in zip(string.ascii_lowercase, row)} 
               for k1,row in zip(string.ascii_lowercase, rows_b)}
    
    # generator function needed to create a lexical closure
    def f_closure(a, b):
        def f(x):
            return a*x + b
        return f
    
    for k1,k2 in product(a_keys.keys(), b_keys.keys()):
        fallback[(k1,k2)] = f_closure(a_keys[k1][k2], b_keys[k1][k2])
    
    return decision, fallback

def load_hierarchical_model(filename):
    
    decision = defaultdict(list)
    fallback = defaultdict(list)
    r_decision = csv.reader(open(filename, 'rt'))
    
    parent_nodes = set()
    for row in r_decision:
        for node in row[1:]:
            parent = [row[0].lower()]
            parent_nodes.add(parent[0])
            while len(parent) > 0:
                decision[node.lower()].append(parent[0])
                parent = decision[parent[0]]
                
    for k in parent_nodes:
        decision.pop(k)
    
    for k in decision.keys():
        for choice in decision[k]:
            fallback[choice].append(k)
    
    return dict(decision), dict(fallback)

def sample_hierarch_fallback_durations(sample, decision_model, fallback_model, min_frequency):
    
    durations = defaultdict(list)
    for e in sample:
        durations[e['key_string']].append(e['release_time'] - e['press_time'])

    n_fallbacks = defaultdict(int)
    fallback_durations = defaultdict(dict)
    for k,choices in decision_model.items():
        for choice in choices:
            if len(durations[k]) < min_frequency and len(fallback_durations[k]) < min_frequency:
                n_fallbacks[k] += 1
                f_durations = []
                for fall_k in sorted(fallback_model[choice]):
                    f_durations.extend(durations[fall_k])
                fallback_durations[k] = f_durations
    
    for k,choice_durations in fallback_durations.items():
        if (len(durations[k]) < min_frequency):
            durations[k] = choice_durations
    
    return dict(durations), n_fallbacks

def hierarchical_fallback_durations(user_samples, decision_model, fallback_model, min_frequency):
    
    user_durations = defaultdict(list)
    user_n_fallbacks = defaultdict(list)
    for user,samples in user_samples.items():
        for s in samples:
            d, n_fallbacks = sample_hierarch_fallback_durations(s, decision_model, fallback_model, min_frequency)
            user_durations[user].append(d)
            user_n_fallbacks[user].append(n_fallbacks)
        
    return dict(user_durations), dict(user_n_fallbacks)

def sample_correlation_fallback_durations(sample, decision_model, fallback_model, min_frequency):
    
    durations = defaultdict(list)
    for e in sample:
        durations[e['key_string']].append(e['release_time'] - e['press_time'])
    
    fallback_durations = defaultdict(dict)
    n_fallbacks = defaultdict(int)
    for k,choices in decision_model.items():
        for choice in choices:
            n_observations = len(durations[k]) + sum(len(d) for d in fallback_durations[k].values())
            if n_observations < min_frequency:
                n_fallbacks[k] += 1
                fallback_durations[k][choice] = [fallback_model[choice, k](x) for x in durations[choice]]
    
    for k,choice_durations in fallback_durations.items():
        for d in choice_durations.values():
            durations[k].extend(d)

    return dict(durations), n_fallbacks

def correlation_fallback_durations(user_samples, decision_model, fallback_model, min_frequency):
    
    user_durations = defaultdict(list)
    user_n_fallbacks = defaultdict(list)
    for user,samples in user_samples.items():
        for s in samples:
            d, n_fallbacks = sample_correlation_fallback_durations(s, decision_model, fallback_model, min_frequency)
            user_durations[user].append(d)
            user_n_fallbacks[user].append(n_fallbacks)
        
    return dict(user_durations), dict(user_n_fallbacks)

os.chdir(WORKING_DIR)
PHYSIOLOGICAL_DECISION, PHYSIOLOGICAL_FALLBACK = \
load_hierarchical_model('physiological-decision-model.csv')

def physiological(user_samples, min_frequency):
    d, n_fallbacks = hierarchical_fallback_durations(user_samples, PHYSIOLOGICAL_DECISION, PHYSIOLOGICAL_FALLBACK, min_frequency)
    return d


if __name__ == '__main__':
    from pprint import pprint
    decision, fallback = load_hierarchical_model('linguistic_durations.csv')
    
    pprint(decision)
    pprint(fallback)