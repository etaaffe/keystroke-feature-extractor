'''
Created on Mar 15, 2013

@author: vinnie
'''

import sys
import math
import string

import numpy as np
import pandas as pd
from IPython import embed
from pprint import pprint
from sklearn import manifold
from itertools import product, combinations
from functools import partial
from scipy.stats import linregress
from collections import defaultdict, Counter
from scipy.spatial.distance import cdist, pdist, squareform

from keycode import *
from bas.rti import pdist_object
# from fspace import normalize
# from features import select_features, concat_features
# from util import listoftuples
# from features import measure_observations, population_outlier_removal, user_outlier_removal

NA = np.NAN

FALLBACK_WEIGHT = 0.25 # weight of fallback observations
T_MIN_FREQUENCY = 3 # min frequency per sample for computing correlation
M_MIN_FREQUENCY = 5 # min frequency per sample for feature fallback

feature_sets = {
'letters': {l:set([l]) for l in letters},

'linguistic': {
    # duration features
    'all_keys': all_keys,
    'letters': letters,
    'vowels': vowels,
    'freq_cons': freq_cons,
    'next_freq_cons': next_freq_cons,
    'least_freq_cons': least_freq_cons,
    'other_cons': other_cons,
    'left_letters': left_letters,
    'right_letters': right_letters,
    'non_letters': non_letters,
    'punctuation': punctuation,
    'punctuation_other': punctuation_other,
    'digits': digits,
    'other_non_letters': other_non_letters,

    'a': set(['a']),
    'e': set(['e']),
    'i': set(['i']),
    'o': set(['o']),
    'u': set(['u']),
    't': set(['t']),
    'n': set(['n']),
    's': set(['s']),
    'r': set(['r']),
    'h': set(['h']),
    'l': set(['l']),
    'd': set(['d']),
    'c': set(['c']),
    'p': set(['p']),
    'f': set(['f']),
    'm': set(['m']),
    'w': set(['w']),
    'y': set(['y']),
    'b': set(['b']),
    'g': set(['g']),
    'space': set(['space']),
    'shift': set(['shift']),
    'period': set(['period']),
    'comma': set(['comma']),
    'quote': set(['quote']),
    
    # transition features, key sets separated by '__' 
    'all_keys__all_keys': all_keys__all_keys,
    'letters__letters': letters__letters,
    'consonants__consonants': consonants__consonants,
    't__h': set(['t__h']),
    's__t': set(['s__t']),
    'n__d': set(['n__d']),
    'vowels__consonants': vowels__consonants,
    'a__n': set(['a__n']),
    'i__n': set(['i__n']),
    'e__r': set(['e__r']),
    'e__s': set(['e__s']),
    'o__n': set(['o__n']),
    'a__t': set(['a__t']),
    'e__n': set(['e__n']),
    'o__r': set(['o__r']),
    'consonants__vowels': consonants__vowels,
    'h__e': set(['h__e']),
    'r__e': set(['r__e']),
    't__i': set(['t__i']),
    'vowels__vowels': vowels__vowels,
    'e__a': set(['e__a']),
    'double__letters': double__letters,
    'left_hand__left_hand': left_hand__left_hand,
    'left_hand__right_hand': left_hand__right_hand,
    'right_hand__left_hand': right_hand__left_hand,
    'right_hand__right_hand': right_hand__right_hand,
    'letters__non_letters': letters__non_letters,
    'letters__space': letters__space,
    'letters__punctuation': letters__punctuation,
    'non_letters__letters': non_letters__letters,
    'shift__letters': shift__letters,
    'space__letters': space__letters,
    'non_letters__non_letters': non_letters__non_letters,
    'space__shift': set(['space__shift']),
    'punctuation__space': punctuation__space
},
            
'linguistic_extended': {
    # duration features
    'all_keys': all_keys,
    'letters': letters,
    'vowels': vowels,
    'freq_cons': freq_cons,
    'next_freq_cons': next_freq_cons,
    'least_freq_cons': least_freq_cons,
    'other_cons': other_cons,
    'left_letters': left_letters,
    'right_letters': right_letters,
    'non_letters': non_letters,
    'punctuation': punctuation,
    'punctuation_other': punctuation_other,
    'digits': digits,
    'other_non_letters': other_non_letters,
     
    'a': set(['a']),
    'e': set(['e']),
    'i': set(['i']),
    'o': set(['o']),
    'u': set(['u']),
    't': set(['t']),
    'n': set(['n']),
    's': set(['s']),
    'r': set(['r']),
    'h': set(['h']),
    'l': set(['l']),
    'd': set(['d']),
    'c': set(['c']),
    'p': set(['p']),
    'f': set(['f']),
    'm': set(['m']),
    'w': set(['w']),
    'y': set(['y']),
    'b': set(['b']),
    'g': set(['g']),
     
    'space': set(['space']),
    'shift': set(['shift']),
    'period': set(['period']),
    'comma': set(['comma']),
    'quote': set(['quote']),
    
    # extended duration
    'left_pointer': left_pointer,
    'left_middle': left_middle,
    'left_index': left_index,
    'left_pinky': left_pinky,
     
    'right_pointer': right_pointer,
    'right_middle': right_middle,
    'right_index': right_index,
    'right_pinky': right_pinky,
     
    'qwe': set('qwe'),
    'asd': set('asd'),
    'zxc': set('zxc'),
    'ert': set('ert'),
    'dfg': set('dfg'),
    'cvb': set('cvb'),
    'yui': set('yui'),
    'hjk': set('hjk'),
    'n_m_comma': {'n','m','comma'},
    'iop': set('iop'),
    'k_l_semicolon': {'k','l','semicolon'},
    'm_comma_period': {'m','comma','period'},
    
    
    # transition features, key sets separated by '-' 
    'all_keys-all_keys': all_keys__all_keys,
    'letters-letters': letters__letters,
    'consonants-consonants': consonants__consonants,
    't-h': set(['t-h']),
    's-t': set(['s-t']),
    'n-d': set(['n-d']),
    'vowels-consonants': vowels__consonants,
    'a-n': set(['a-n']),
    'i-n': set(['i-n']),
    'e-r': set(['e-r']),
    'e-s': set(['e-s']),
    'o-n': set(['o-n']),
    'a-t': set(['a-t']),
    'e-n': set(['e-n']),
    'o-r': set(['o-r']),
    'consonants-vowels': consonants__vowels,
    'h-e': set(['h-e']),
    'r-e': set(['r-e']),
    't-i': set(['t-i']),
    'vowels-vowels': vowels__vowels,
    'e-a': set(['e-a']),
    'double-letters': double__letters,
    'left_hand-left_hand': left_hand__left_hand,
    'left_hand-right_hand': left_hand__right_hand,
    'right_hand-left_hand': right_hand__left_hand,
    'right_hand-right_hand': right_hand__right_hand,
    'letters-non_letters': letters__non_letters,
    'letters-space': letters__space,
    'letters-punctuation': letters__punctuation,
    'non_letters-letters': non_letters__letters,
    'shift-letters': shift__letters,
    'space-letters': space__letters,
    'non_letters-non_letters': non_letters__non_letters,
    'space-shift': set(['space-shift']),
    'punctuation-space': punctuation__space,
    
    # extended transition
    'left_pointer-left_pointer': left_pointer__left_pointer,
    'left_pointer-left_middle': left_pointer__left_middle,
    'left_pointer-left_index': left_pointer__left_index,
    'left_pointer-left_pinky': left_pointer__left_pinky,
    'left_pointer-right_pointer': left_pointer__right_pointer,
    'left_pointer-right_middle': left_pointer__right_middle,
    'left_pointer-right_index': left_pointer__right_index,
    'left_pointer-right_pinky': left_pointer__right_pinky,
    
    'left_middle-left_pointer': left_middle__left_pointer,
    'left_middle-left_middle': left_middle__left_middle,
    'left_middle-left_index': left_middle__left_index,
    'left_middle-left_pinky': left_middle__left_pinky,
    'left_middle-right_pointer': left_middle__right_pointer,
    'left_middle-right_middle': left_middle__right_middle,
    'left_middle-right_index': left_middle__right_index,
    'left_middle-right_pinky': left_middle__right_pinky,
    
    'left_index-left_pointer': left_index__left_pointer,
    'left_index-left_middle': left_index__left_middle,
    'left_index-left_index': left_index__left_index,
    'left_index-left_pinky': left_index__left_pinky,
    'left_index-right_pointer': left_index__right_pointer,
    'left_index-right_middle': left_index__right_middle,
    'left_index-right_index': left_index__right_index,
    'left_index-right_pinky': left_index__right_pinky,
    
    'left_pinky-left_pointer': left_pinky__left_pointer,
    'left_pinky-left_middle': left_pinky__left_middle,
    'left_pinky-left_index': left_pinky__left_index,
    'left_pinky-left_pinky': left_pinky__left_pinky,
    'left_pinky-right_pointer': left_pinky__right_pointer,
    'left_pinky-right_middle': left_pinky__right_middle,
    'left_pinky-right_index': left_pinky__right_index,
    'left_pinky-right_pinky': left_pinky__right_pinky,
    
    'right_pointer-left_pointer': right_pointer__left_pointer,
    'right_pointer-left_middle': right_pointer__left_middle,
    'right_pointer-left_index': right_pointer__left_index,
    'right_pointer-left_pinky': right_pointer__left_pinky,
    'right_pointer-right_pointer': right_pointer__right_pointer,
    'right_pointer-right_middle': right_pointer__right_middle,
    'right_pointer-right_index': right_pointer__right_index,
    'right_pointer-right_pinky': right_pointer__right_pinky,
    
    'right_middle-left_pointer': right_middle__left_pointer,
    'right_middle-left_middle': right_middle__left_middle,
    'right_middle-left_index': right_middle__left_index,
    'right_middle-left_pinky': right_middle__left_pinky,
    'right_middle-right_pointer': right_middle__right_pointer,
    'right_middle-right_middle': right_middle__right_middle,
    'right_middle-right_index': right_middle__right_index,
    'right_middle-right_pinky': right_middle__right_pinky,
    
    'right_index-left_pointer': right_index__left_pointer,
    'right_index-left_middle': right_index__left_middle,
    'right_index-left_index': right_index__left_index,
    'right_index-left_pinky': right_index__left_pinky,
    'right_index-right_pointer': right_index__right_pointer,
    'right_index-right_middle': right_index__right_middle,
    'right_index-right_index': right_index__right_index,
    'right_index-right_pinky': right_index__right_pinky,
    
    'right_pinky-left_pointer': right_pinky__left_pointer,
    'right_pinky-left_middle': right_pinky__left_middle,
    'right_pinky-left_index': right_pinky__left_index,
    'right_pinky-left_pinky': right_pinky__left_pinky,
    'right_pinky-right_pointer': right_pinky__right_pointer,
    'right_pinky-right_middle': right_pinky__right_middle,
    'right_pinky-right_index': right_pinky__right_index,
    'right_pinky-right_pinky': right_pinky__right_pinky,

},
}


fallback_hierarchies = {
'one_level': {
    'letters': list(string.ascii_lowercase)},

'letters_physiological': {
    'letters': ['left_letters','right_letters'],
    'left_letters': list('qwertasdfgzxcvb'),
    'right_letters': list('yuiophjklnm'),
},

'letters_linguistic': {
    'letters': ['vowels','consonants'],
    'vowels': list('aeiou'),
    'consonants': ['freq_cons', 'next_freq_cons', 'least_freq_cons', 'other_cons'],
    'freq_cons': list('tnsrh'),
    'next_freq_cons': list('ldcpf'),
    'least_freq_cons': list('mwybg'),
    'other_cons': list('jkqvxz'),
},

'linguistic': {
    # duration fallback
    'all_keys': ['letters', 'non_letters'],
    'letters': ['vowels', 'freq_cons', 'next_freq_cons', 'least_freq_cons', 'left_letters', 'right_letters'],
    'vowels': vowels,
    'freq_cons': freq_cons,
    'next_freq_cons': next_freq_cons,
    'least_freq_cons': least_freq_cons.union(set(['other_cons'])), # not a typo, 'other_cons' is a leaf node
     
    'non_letters': ['digits', 'other_non_letters', 'punctuation', 'space', 'shift'],
    'punctuation': ['punctuation_other', 'period', 'comma', 'quote'],
    
    #transition fallback
    'all_keys__all_keys': ['letters__letters', 'letters__non_letters', 'non_letters__letters', 'non_letters__non_letters'],
    'letters__letters': ['consonants__consonants', 'vowels__consonants', 'consonants__vowels', 'vowels__vowels', 'double__letters', 'left_hand__left_hand', 'left_hand__right_hand', 'right_hand__left_hand', 'right_hand__right_hand'],
     
    'consonants__consonants': ['t-h', 's-t', 'n-d'],
    'vowels__consonants': ['a-n', 'i-n', 'e-r', 'e-s', 'o-n', 'a-t', 'e-n', 'o-r'],
    'consonants__vowels': ['h-e', 'r-e', 't-i'],
    'vowels__vowels': ['e-a',],
     
    'letters__non_letters': ['letters__space', 'letters__punctuation'],
    'non_letters__letters': ['shift__letters', 'space__letters'],
    'non_letters__non_letters': ['space__shift', 'punctuation__space']
},
            
'linguistic_extended': {
    # duration fallback
    'all_keys': ['letters', 'non_letters'],
    'letters': ['vowels', 'freq_cons', 'next_freq_cons', 'least_freq_cons', 'left_letters', 'right_letters'],
    'vowels': vowels,
    'freq_cons': freq_cons,
    'next_freq_cons': next_freq_cons,
    'least_freq_cons': least_freq_cons.union(set(['other_cons'])), # not a typo, 'other_cons' is a leaf node
     
    'non_letters': ['digits', 'other_non_letters', 'punctuation', 'space', 'shift'],
    'punctuation': ['punctuation_other', 'period', 'comma', 'quote'],
    
    #transition fallback
    'all_keys-all_keys': ['letters-letters', 'letters-non_letters', 'non_letters-letters', 'non_letters-non_letters'],
    'letters-letters': ['consonants-consonants', 'vowels-consonants', 'consonants-vowels', 'vowels-vowels', 'double-letters', 'left_hand-left_hand', 'left_hand-right_hand', 'right_hand-left_hand', 'right_hand-right_hand'],
     
    'consonants-consonants': ['t-h', 's-t', 'n-d'],
    'vowels-consonants': ['a-n', 'i-n', 'e-r', 'e-s', 'o-n', 'a-t', 'e-n', 'o-r'],
    'consonants-vowels': ['h-e', 'r-e', 't-i'],
    'vowels-vowels': ['e-a',],
     
    'letters-non_letters': ['letters-space', 'letters-punctuation'],
    'non_letters-letters': ['shift-letters', 'space-letters'],
    'non_letters-non_letters': ['space-shift', 'punctuation-space'],
    
    'left_letters': ['left_pointer','left_middle','left_index','left_pinky','qwe','asd','zxc','ert','dfg','cvb'],
    'right_letters': ['right_pointer','right_middle','right_index','right_pinky','yui','hjk','n_m_comma','k_l_semicolon','m_comma_period'],
    
    'left_hand-left_hand': ['left_pointer-left_pointer',
                            'left_pointer-left_middle',
                            'left_pointer-left_index',
                            'left_pointer-left_pinky',
                            'left_middle-left_pointer',
                            'left_middle-left_middle',
                            'left_middle-left_index',
                            'left_middle-left_pinky',
                            'left_index-left_pointer',
                            'left_index-left_middle',
                            'left_index-left_index',
                            'left_index-left_pinky',
                            'left_pinky-left_pointer',
                            'left_pinky-left_middle',
                            'left_pinky-left_index',
                            'left_pinky-left_pinky',],
                        
    'left_hand-right_hand': ['left_pointer-right_pointer',
                            'left_pointer-right_middle',
                            'left_pointer-right_index',
                            'left_pointer-right_pinky',
                            'left_middle-right_pointer',
                            'left_middle-right_middle',
                            'left_middle-right_index',
                            'left_middle-right_pinky',
                            'left_index-right_pointer',
                            'left_index-right_middle',
                            'left_index-right_index',
                            'left_index-right_pinky',
                            'left_pinky-right_pointer',
                            'left_pinky-right_middle',
                            'left_pinky-right_index',
                            'left_pinky-right_pinky',],
                        
    'right_hand-left_hand': ['right_pointer-left_pointer',
                            'right_pointer-left_middle',
                            'right_pointer-left_index',
                            'right_pointer-left_pinky',
                            'right_middle-left_pointer',
                            'right_middle-left_middle',
                            'right_middle-left_index',
                            'right_middle-left_pinky',
                            'right_index-left_pointer',
                            'right_index-left_middle',
                            'right_index-left_index',
                            'right_index-left_pinky',
                            'right_pinky-left_pointer',
                            'right_pinky-left_middle',
                            'right_pinky-left_index',
                            'right_pinky-left_pinky',],
    
    'right_hand-right_hand': ['right_pointer-right_pointer',
                            'right_pointer-right_middle',
                            'right_pointer-right_index',
                            'right_pointer-right_pinky',
                            'right_middle-right_pointer',
                            'right_middle-right_middle',
                            'right_middle-right_index',
                            'right_middle-right_pinky',
                            'right_index-right_pointer',
                            'right_index-right_middle',
                            'right_index-right_index',
                            'right_index-right_pinky',
                            'right_pinky-right_pointer',
                            'right_pinky-right_middle',
                            'right_pinky-right_index',
                            'right_pinky-right_pinky'],

},
}

def transition_digrams(df, distance=1):
    a = df.groupby(['user','session']).apply(lambda x: x[:-distance].reset_index())
    b = df.groupby(['user','session']).apply(lambda x: x[distance:].reset_index())
    
    a = a[['user','session','keyname','timepress','timerelease']]
    b = b[['keyname','timepress','timerelease']]
    
    a.columns = ['user','session','keyname_1', 'timepress_1', 'timerelease_1']
    b.columns = ['keyname_2', 'timepress_2', 'timerelease_2']
    
    joined = pd.concat([a,b], join='inner', axis=1)
    
    cols=['user','session','keynames','transition']
    
    # Create columns for each transition type
    t1 = pd.DataFrame({'user': joined['user'], 
                       'session': joined['session'],
                       'keynames': joined['keyname_1'] + '__' + joined['keyname_2'],
                       'transition': joined['timepress_2'] - joined['timerelease_1']},
                      columns=cols, index=joined.index)
    
    t2 = pd.DataFrame({'user': joined['user'], 
                       'session': joined['session'],
                       'keynames': joined['keyname_1'] + '__' + joined['keyname_2'],
                       'transition': joined['timepress_2'] - joined['timepress_1']},
                      columns=cols, index=joined.index)
    
    return t1, t2

def outlier_removal(df, col_name, group_cols, distance=4, max_iterations=10):
    '''
    Remove duration outliers on a per-user basis
    
    10 iterations will remove most outliers.
    
    Does the following:
        group df by user and keyname
        get mean and std for each group (user/keyname combination)
        filter df durations with the corresponding user/key mean and stds
    
    This could be more efficient by testing the number of outliers removed for 
    each group and only recomputing the groups with more than 0 removed
    '''
    for i in range(max_iterations):
        prev_len = len(df)
        
        user_keys = df.groupby(group_cols)[col_name]
        m = user_keys.mean()
        s = user_keys.std()
        f = lambda x: (x[col_name] > m[x[group_cols]] - distance * s[x[group_cols]]) \
                    & (x[col_name] < m[x[group_cols]] + distance * s[x[group_cols]])
        
        df = df[df.apply(f, axis=1)]
        
        print('Removed %d observations' %(prev_len - len(df)))
        
        if len(df) == prev_len:
            # no more outliers have been removed
            break 
    
    return df

def extract_features(df, group_col_name, feature_col_name, features, decisions, feature_name_prefix):
    
    feature_vector = {}
    for feature_name,feature_set in features.items():
        full_feature_name = '%s%s' %(feature_name_prefix, feature_name)
        obs = df[df[group_col_name].isin(feature_set)][feature_col_name]
        
        if len(obs) < M_MIN_FREQUENCY and feature_name in decisions.keys():
            fallback_name =  decisions[feature_name]
            
#             fallback_obs = pd.DataFrame() # Start with an empty frame
#             while len(obs) + len(fallback_obs) < M_MIN_FREQUENCY:
#                 fallback_set = globals()[fallback_name]
#                 fallback_obs = df[df[group_col_name].isin(fallback_set)][feature_col_name]
#                 
#                 if fallback_name in decisions.keys():
#                     fallback_name = decisions[fallback_name] # go up the tree
#                 else:
#                     break # reached the root node

            fallback_obs = pd.DataFrame() # Start with an empty frame
            while len(obs) + len(fallback_obs) < M_MIN_FREQUENCY:
                fallback_set = globals()[fallback_name]
                fallback_obs = df[df[group_col_name].isin(fallback_set)][feature_col_name]
                
                if fallback_name in decisions.keys():
                    fallback_name = decisions[fallback_name] # go up the tree
                else:
                    break # reached the root node
            
#             obs = obs.append(fallback_obs)
            n = len(obs)
            
            # Prevent NA values
            if n == 0:
                obs_mean = 0
                obs_std = 0
            elif n == 1:
                obs_mean = obs.mean()
                obs_std = 0
            else:
                obs_mean = obs.mean()
                obs_std = obs.std()
            
            feature_vector['%s.mean' %full_feature_name] = (n*obs_mean + FALLBACK_WEIGHT*fallback_obs.mean())/(n + FALLBACK_WEIGHT)
            feature_vector['%s.std' %full_feature_name] = (n*obs_std + FALLBACK_WEIGHT*fallback_obs.std())/(n + FALLBACK_WEIGHT)
#             feature_vector['%s.mean' %full_feature_name] = obs_mean
#             feature_vector['%s.std' %full_feature_name] = obs_std
        else:
            feature_vector['%s.mean' %full_feature_name] = obs.mean()
            feature_vector['%s.std' %full_feature_name] = obs.std()
    
    return pd.Series(feature_vector)
    
def reverse_tree(features, hierarchy):
    parents = {}
    
    for parent,children in hierarchy.items():
        for child in children:
            parents[child] = parent
    
    return parents
    
def hierarchy_fallback_features(df, features_name='linguistic', fallback_name='linguistic'):

    features = feature_sets[features_name]
    fallback = fallback_hierarchies[fallback_name]
    
    duration_features = {k:v for k,v in features.items() if '__' not in k}
    transition_features = {k:v for k,v in features.items() if '__' in k}
    decisions = reverse_tree(features, fallback)
    
    du_features = pd.DataFrame()
    if len(duration_features) > 0:
        du = pd.DataFrame({'user': df['user'], 
                          'session': df['session'],
                          'keyname': df['keyname'],
                          'duration': df['timerelease'] - df['timepress']},
                          columns=['user','session','keyname','duration']) 
        du = outlier_removal(du, 'duration', 'keyname')
        
        du_features = du.groupby(['user','session']).apply(lambda x:
                                            extract_features(x, feature_col_name='duration',
                                                                group_col_name='keyname',
                                                                features=duration_features,
                                                                decisions=decisions,
                                                                feature_name_prefix='du_'))
    
    t1_features, t2_features = pd.DataFrame(), pd.DataFrame()
    if len(transition_features) > 0:
        t1,t2 = transition_digrams(df)
        t1 = outlier_removal(t1, 'transition', 'keynames')
        t2 = outlier_removal(t2, 'transition', 'keynames')
    
        t1_features = t1.groupby(['user','session']).apply(lambda x:
                                        extract_features(x, feature_col_name='transition',
                                                            group_col_name='keynames',
                                                            features=transition_features,
                                                            decisions=decisions,
                                                            feature_name_prefix='t2_'))

        t2_features = t2.groupby(['user','session']).apply(lambda x:
                                        extract_features(x, feature_col_name='transition',
                                                            group_col_name='keynames',
                                                            features=transition_features,
                                                            decisions=decisions,
                                                            feature_name_prefix='t2_'))
    
    fspace = pd.concat([du_features, t1_features, t2_features], axis=1)
    
    return fspace

def extract_features_regression(df, group_col_name, feature_col_name, features, decisions, fallback_fns, feature_name_prefix):

    feature_vector = {}
    for feature_name,feature_set in features.items():
        full_feature_name = '%s%s' %(feature_name_prefix, feature_name)
        obs = df[df[group_col_name].isin(feature_set)][feature_col_name]
        
        if len(obs) < M_MIN_FREQUENCY:
            fallback_features = decisions[feature_name]
            
#             fallback_obs = pd.Series() # Start with an empty frame
#             for fallback_name in fallback_features:
#                 raw_obs = df[df[group_col_name].isin(features[fallback_name].difference(feature_set))][feature_col_name]
#                 transformed = fallback_fns[feature_name][fallback_name](raw_obs)
#                 fallback_obs = fallback_obs.append(transformed)
#                 
#                 if len(obs) + len(fallback_obs) >= M_MIN_FREQUENCY:
#                     break

            for fallback_name in fallback_features:
                raw_obs = df[df[group_col_name].isin(features[fallback_name].difference(feature_set))][feature_col_name]
                transformed = fallback_fns[feature_name][fallback_name](raw_obs)
                obs = obs.append(transformed)
                
                if len(obs) >= M_MIN_FREQUENCY:
                    break
            
            n = len(obs)
            
            # Prevent NA values
            if n == 0:
                obs_mean = 0
                obs_std = 0
            elif n == 1:
                obs_mean = obs.mean()
                obs_std = 0
            else:
                obs_mean = obs.mean()
                obs_std = obs.std()
            
#             feature_vector['%s.mean' %full_feature_name] = (n*obs_mean + FALLBACK_WEIGHT*fallback_obs.mean())/(n + FALLBACK_WEIGHT)
#             feature_vector['%s.std' %full_feature_name] = (n*obs_std + FALLBACK_WEIGHT*fallback_obs.std())/(n + FALLBACK_WEIGHT)
            feature_vector['%s.mean' %full_feature_name] = obs_mean
            feature_vector['%s.std' %full_feature_name] = obs_std
        else:
            feature_vector['%s.mean' %full_feature_name] = obs.mean()
            feature_vector['%s.std' %full_feature_name] = obs.std()
    
    return pd.Series(feature_vector)

def make_lin_transform(slope, intercept):
    '''
    Closure which captures the slope and intercept to create a linear 
    transformation function.
    '''
    def lin_transform(x):
        return x*slope + intercept
    return lin_transform

def correlation_decisions(cor, feature_names, max_decisions=5):
    
    decisions = {}
    for feature_name in feature_names:
        # find the closest correlated features
        possibilities = cor[cor['feature_1']==feature_name].sort('corrcoef', ascending=False)
        decisions[feature_name] = list(possibilities[:max_decisions]['feature_2'])
    
    functions = defaultdict(dict)
    for idx, row in cor.iterrows():
        functions[row['feature_1']][row['feature_2']] = make_lin_transform(row['slope'], row['intercept'])
        
    return decisions, functions

def regression_fallback_features(df, d_cor, t1_cor, t2_cor, features):
    
    duration_features = {k:v for k,v in features.items() if '-' not in k}
    transition_features = {k:v for k,v in features.items() if '-' in k}
    
    du_features = pd.DataFrame()
    if len(duration_features) > 0:
        du = pd.DataFrame({'user': df['user'], 
                          'session': df['session'],
                          'keyname': df['keyname'],
                          'duration': df['timerelease'] - df['timepress']},
                          columns=['user','session','keyname','duration']) 
        du = outlier_removal(du, 'duration', 'keyname')
        du_dec, du_fns = correlation_decisions(d_cor, duration_features.keys())
        du_feature_fn = partial(extract_features_regression,
                                      feature_col_name='duration',
                                      group_col_name='keyname',
                                      features=duration_features, 
                                      decisions=du_dec, 
                                      fallback_fns=du_fns,
                                      feature_name_prefix='du_')
        
        du_features = du.groupby(['user','session']).apply(du_feature_fn)

    t1_features, t2_features = pd.DataFrame(), pd.DataFrame()
    if len(transition_features) > 0:
        t1,t2 = transition_digrams(df)
        
        t1 = outlier_removal(t1, 'transition', 'keynames')
        t2 = outlier_removal(t2, 'transition', 'keynames')
        
        t1_dec, t1_fns = correlation_decisions(t1_cor, transition_features.keys())
        t2_dec, t2_fns = correlation_decisions(t2_cor, transition_features.keys())
        
        t1_feature_fn = partial(extract_features_regression,
                                      feature_col_name='transition',
                                      group_col_name='keynames',
                                      features=transition_features, 
                                      decisions=t1_dec, 
                                      fallback_fns=t1_fns,
                                      feature_name_prefix='t1_')
        
        t2_feature_fn = partial(extract_features_regression,
                                      feature_col_name='transition',
                                      group_col_name='keynames',
                                      features=transition_features, 
                                      decisions=t2_dec, 
                                      fallback_fns=t2_fns,
                                      feature_name_prefix='t2_')
        
        t1_features = t1.groupby(['user','session']).apply(t1_feature_fn)
        t2_features = t2.groupby(['user','session']).apply(t2_feature_fn)
    
    fspace = pd.concat([du_features, t1_features, t2_features], axis=1)
    
    return fspace, 0
    
def mutual_exclusive_observations(df, features, feature_1, feature_2, col_name):
    '''
    Return observations of feature 1 and observations of feature 2 which
    do not appear in feature 1.
    '''
    
    obs_1 = df[df[col_name].isin(features[feature_1])]
    obs_2 = df[df[col_name].isin(features[feature_2].difference(features[feature_1]))]
    
    obs_1 = obs_1.groupby('session').filter(lambda x: len(x['user']) > T_MIN_FREQUENCY)
    obs_2 = obs_2.groupby('session').filter(lambda x: len(x['user']) > T_MIN_FREQUENCY)
    
    n_obs_1 = len(obs_1)
    n_obs_2 = len(obs_2)
    
    o1 = obs_1.groupby(['user','session']).apply(lambda x: x.reset_index())
    o2 = obs_2.groupby(['user','session']).apply(lambda x: x.reset_index())
    n_cooccurrences = len(pd.concat([o1,o2], axis=1, join='inner'))
    
    obs_1 = obs_1.groupby(['user','session']).mean()
    obs_2 = obs_2.groupby(['user','session']).mean()
    
    return obs_1, obs_2, n_obs_1, n_obs_2, n_cooccurrences

def features_correlation(df, features, col_name):
    regress = {}
    for feature_1, feature_2 in product(features.keys(), features.keys()):
        
        # Don't try regression when feature 2 is a subset of feature 1.
        # Observations in feature 1 already contain all of feature 2 values
        if features[feature_2].issubset(features[feature_1]):
            continue
        
        obs_1, obs_2, n_obs_1, n_obs_2, n_cooccurrences = mutual_exclusive_observations(df, features, feature_1, feature_2, col_name)
        
        co = pd.DataFrame(pd.concat([obs_1, obs_2], axis=1, join='inner'))
        co.columns = ['obs_1', 'obs_2']
        
        if len(co) < 3:
            print 'Warning: a set of features had less than 3 mean cooccurrences:', feature_1, feature_2
            print co.head()
            continue
        
        print 'Regression for', feature_1, feature_2
        regress[(feature_1, feature_2)] = list(linregress(co)) + [n_obs_1, n_obs_2, n_cooccurrences]
        
    idx = pd.MultiIndex.from_tuples(regress.keys())
    values = [regress[i] for i in idx]
    regress = pd.DataFrame(data=values, index=idx, columns=['slope', 'intercept', 'r', 'p', 'stderr', 'n_obs_1', 'n_obs_2', 'n_cooccurrences'])
    regress.index.names = ['feature_1', 'feature_2']
    return regress

def feature_set_correlation(df, features):
    '''
    Perform a linear regression for observations with enough cooccurrences (min_frequency).
    
    Both types look for cooccurrences across different samples.
    'population' type performs linear regression for the entire population
    'user' type performs linear regression for each individual user
    
    'individual' uses the individual observations in the regression
    'mean' uses the mean value in each sample for regression  
    
    This function could be improved a lot.
    '''

    duration_features = {k:v for k,v in features.items() if '-' not in k}
    transition_features = {k:v for k,v in features.items() if '-' in k}
    
    du_cor = None
    if len(duration_features) > 0:
        du = pd.DataFrame({'user': df['user'], 
                          'session': df['session'],
                          'keyname': df['keyname'],
                          'duration': df['timerelease'] - df['timepress']},
                          columns=['user','session','keyname','duration'])
        
        du = outlier_removal(du, 'duration', 'user', distance=2)
        
        du_cor = features_correlation(du, duration_features, 'keyname')
        du_cor['corrcoef'] = du_cor['r']**2
        du_cor = du_cor.sort('corrcoef', ascending=False)
    
    t1_cor, t2_cor = None, None
    if len(transition_features) > 0:
        t1,t2 = transition_digrams(df)
        t1 = outlier_removal(t1, 'transition', 'user')
        t2 = outlier_removal(t2, 'transition', 'user')
        t1_cor = features_correlation(t1, transition_features, 'keynames')
        t2_cor = features_correlation(t2, transition_features, 'keynames')
        t1_cor['corrcoef'] = t1_cor['r']**2
        t2_cor['corrcoef'] = t2_cor['r']**2
        t1_cor = t1_cor.sort('corrcoef', ascending=False)
        t2_cor = t2_cor.sort('corrcoef', ascending=False)
    
    return du_cor, t1_cor, t2_cor

def reduce_keystrokes(fname, sizes):
    
    for n in sizes:
        df = pd.read_csv(fname, usecols=['user','session','timepress','timerelease','keycode','keyname'])
        df = df.set_index(['user','session']).groupby(level=[0,1]).apply(lambda x: x[:n]).reset_index([0,1], drop=True)
        df.to_csv(fname[:-4]+'_%d.csv' %n)
        
    return

def input_speed(df):
    
    return

def unstack_events(df):
    
    presses = df[['timepress','keyname']]
    presses['keyname'] = '+' + presses['keyname']
    presses.columns = ['time','action']
    
    releases = df[['timerelease','keyname']]
    releases['keyname'] = '-' + releases['keyname']
    releases.columns = ['time','action']
    
    df = pd.concat([presses,releases]).sort(['time'])
    
    return df

def keystroke_action_digrams(df):
    
    presses = df[['timepress','keyname']]
    presses['keyname'] = '+' + presses['keyname']
    presses.columns = ['time','action']
    
    releases = df[['timerelease','keyname']]
    releases['keyname'] = '-' + releases['keyname']
    releases.columns = ['time','action']
    
    actions = pd.concat([presses,releases]).sort(['time'])
    # Make key-action digram strings
    digrams = actions.groupby(level=[0,1]).apply(lambda x: pd.Series(x['action'][:-1] + ' ' + x['action'][1:]))
    
    digram_freqs = digrams.groupby(level=[0,1]).apply(Counter)
    
    return digram_freqs

def histogram_distance(df1, df2):
    # TODO: normalize each histogram before computing the distance?
    diff = (df1/df1.sum() - df2/df2.sum()).abs()
    diff[diff.isnull()] = df1[diff.isnull()]
    diff[diff.isnull()] = df2[diff.isnull()]
    
    return np.linalg.norm(diff)

def key_action_mds(df):
    
    df = keystroke_action_digrams(df)
    
    df_groups = df.groupby(level=[0,1])
    users, freqs = [], []
    for name,group in df_groups:
        users.append(name[0])
        freqs.append(group)
    
    D = pdist_object(freqs, histogram_distance)
    similarities = squareform(D)
    embed()
    s = pd.DataFrame(similarities, index=users, columns=users)
    print(bs_to_ws(s, k))
#     similarities = squareform([histogram_distance(xi,yi) for i,yi in enumerate(freqs) for xi in freqs[i+1:]])
    
    for l in range(2, 20):
        mds = manifold.MDS(n_components=l, max_iter=300, n_jobs=2, dissimilarity="precomputed")
        pos = mds.fit(similarities).embedding_
        g = pdist(pos)
        k = kruskals(D, g)
        print(l, k)
        user_features = pd.DataFrame(pos, index=users)
        
    return user_features

def normalize(df, method='minmax', distance=2):
    
    if method == 'stddev':
        for col in df.columns - ['user', 'session']:
            upper = df[col].mean() + distance*df[col].std()
            lower = df[col].mean() - distance*df[col].std()
            df[col] = (df[col] - lower)/(upper - lower)
            df[col][df[col] < 0] = 0
            df[col][df[col] > 1] = 1
    elif method == 'minmax':
        for col in df.columns - ['user', 'session']:
            df[col] = (df[col] - df[col].min())/(df[col].max() - df[col].min())
        
    return df

def var_length_features(train, features_name, fallback_names, fnames):
    
    for fallback_name in fallback_names:
        if fallback_name == 'regression':
            
            for fspace_name in fnames:
                df = pd.read_csv('../../../data/keystroke/%s.csv' %fspace_name)
                du_cor = pd.read_csv('../../../feature-correlation/letters/Stewart_du.csv')
                fspace, num_fallbacks = regression_fallback_features(df, du_cor, None, None, features[features_name])
                fspace_norm = normalize(fspace)
                fspace_norm.to_csv('../../../features/keystroke/fallback-models/%s/%s.csv' %(fallback_name, fspace_name))
        else:
            for fspace_name in fnames:
                df = pd.read_csv('../../../data/keystroke/%s.csv' %fspace_name)
                fspace, num_fallbacks = hierarchy_fallback_features(df, feature_sets[features_name], fallback_hierarchies[fallback_name])
                fspace_norm = normalize(fspace)
                fspace_norm.to_csv('../../../features/keystroke/fallback-models/%s/%s.csv' %(fallback_name, fspace_name))
        
# from traceback import print_exception
# def excepthook(type, value, traceback):
#     print_exception(type, value, traceback)
#     embed()
# import sys
# sys.excepthook = excepthook

def sort_keystrokes(fname):
    df = pd.read_csv(fname)
    df = df.groupby(['user','session']).apply(lambda x: x.sort('timepress'))
    df = df[['user','session','timepress','timerelease','keycode','keyname']].reset_index(drop=True)
    return df

def test_key_action_mds(df):
    
    df = key_action_mds(df)
    df.to_csv('/home/vinnie/tmp/keyaction.csv')
    
    return

import re;

def match(x, y):
    s=lambda x,y:re.search('.*'.join(x),y)
    return s(x, y) is not None

def clean_features(df):
    
    df[(df==np.inf)|(df==-np.inf)|(np.isnan(df))] = 0
    return df

if __name__ == '__main__':
    df = pd.read_csv('/home/vinnie/Dropbox/data/villani-keystroke/14user.csv')
    fspace = hierarchy_fallback_features(df)
    fspace.to_csv('/home/vinnie/tmp/14user-bas.csv')
    stdnorm = normalize(fspace, 'stddev')
    stdnorm = clean_features(stdnorm)
    stdnorm.to_csv('/home/vinnie/tmp/14user-bas_std-norm.csv')
    minmaxnorm = normalize(fspace)
    minmaxnorm = clean_features(minmaxnorm)
    minmaxnorm.to_csv('/home/vinnie/tmp/14user-bas_minmax-norm.csv')
#     df = pd.read_csv('/home/vinnie/workspace/data/keystroke/%s.csv' %train)
#     test_key_action_mds(df)
    
#     keystroke_action_digrams(df)
    
#     du_cor, t1_cor, t2_cor = feature_set_correlation(df, features[features_name])
#     du_cor.to_csv('../../../feature-correlation/%s/%s_du.csv' %(features_name, train))
#     t1_cor.to_csv('../../../feature-correlation/%s/%s_t1.csv' %(features_name, train))
#     t2_cor.to_csv('../../../feature-correlation/%s/%s_t2.csv' %(features_name, train))
    
#     var_length_features(train, features_name, ['regression','letters_physiological'], ['Villani_30user_50', 'Villani_30user_100']) #,'Villani_30user_200','Villani_30user_300','Villani_30user_400','Villani_30user_500','Villani_30user'])
#     var_length_features(train, features_name, ['regression','letters_linguistic','letters_physiological','one_level'], ['Villani_30user_50', 'Villani_30user_100','Villani_30user_200','Villani_30user_300','Villani_30user_400','Villani_30user_500','Villani_30user'])
    
    
#     var_length_features(train, features_name, ['Villani_14user_50','Villani_14user_100','Villani_14user_200','Villani_14user_300','Villani_14user_400','Villani_14user_500','Villani_14user'])
#     var_length_features(train, features_name, ['Villani_119user_50','Villani_119user_100','Villani_119user_200','Villani_119user_300','Villani_119user_400','Villani_119user_500','Villani_119user'])
