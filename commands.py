'''
Created on Apr 10, 2013

@author: vinnie
'''

import os
import pandas as pd
from argparse import ArgumentParser

import keystroke as kfe

load_data = lambda filename, index_cols: pd.read_csv(filename, index_col=index_cols)

def extract(args):
    '''
    Run and obtain authentication results on a reg set against a ref set.
    '''

    parser = ArgumentParser(prog='%s %s' % ("kfe", "extract"))
    parser.add_argument('input', help='Raw keystroke data file')
    parser.add_argument('output', help='Feature file output')
    parser.add_argument('--features', help='Feature set', default='linguistic')
    parser.add_argument('--measure', help='Feature measure function', default='gaussian')
    parser.add_argument('--fallback', help='Fallback hierarchy', default='linguistic')
    parser.add_argument('--normalization', help='Normalization method', default='stddev')
    parser.add_argument('--norm_distance', type=int, help='Std distance for std normalivation', default=2)
    parser.add_argument('--groupby', help='Groupby columns', default='user,session')
    args = parser.parse_args(args)
    
    index_cols = args.groupby.split(',')
    df = kfe.extract_keystroke_features(load_data(args.input, index_cols), index_cols, args.features, args.fallback, args.measure)
    
    if args.normalization != 'none':
        df = kfe.normalize(df, args.normalization, args.norm_distance)
    
    df.to_csv(args.output)
    return

def extract_meta(args):
    '''
    Run and obtain authentication results on a reg set against a ref set.
    '''

    parser = ArgumentParser(prog='%s %s' % ("kfe", "extract"))
    parser.add_argument('metainput', help='Raw keystroke data file')
    parser.add_argument('input', help='Raw keystroke data file')
    parser.add_argument('metaoutput', help='Feature file output')
    parser.add_argument('output', help='Feature file output')
    parser.add_argument('--features', help='Feature set', default='linguistic')
    parser.add_argument('--measure', help='Feature measure function', default='gaussian')
    parser.add_argument('--fallback', help='Fallback hierarchy', default='linguistic')
    parser.add_argument('--normalization', help='Normalization method', default='stddev')
    parser.add_argument('--norm_distance', type=int, help='Std distance for std normalivation', default=2)
    parser.add_argument('--groupby', help='Groupby columns', default='user,session')
    args = parser.parse_args(args)
    
    index_cols = args.groupby.split(',')
    dfmeta, df = kfe.extract_keystroke_features_meta(load_data(args.metainput, index_cols), load_data(args.input, index_cols), index_cols, args.features, args.fallback, args.measure)
    
    if args.normalization != 'none':
        dfmeta, df = kfe.normalize_meta(dfmeta, df, args.normalization, args.norm_distance)
    
    dfmeta.to_csv(args.metaoutput)
    df.to_csv(args.output)
    return

def extract_hash(args):
    '''
    Run and obtain authentication results on a reg set against a ref set.
    '''

    parser = ArgumentParser(prog='%s %s' % ("kfe", "extract"))
    parser.add_argument('input', help='Raw keystroke data file')
    parser.add_argument('output', help='Feature file output')
    parser.add_argument('--num_features', help='Number of features', type=int, default=200)
    parser.add_argument('--normalization', help='Normalization method', default='stddev')
    parser.add_argument('--norm_distance', type=int, help='Std distance for std normalivation', default=2)
    parser.add_argument('--groupby', help='Groupby columns', default='user,session')
    args = parser.parse_args(args)
    
    index_cols = args.groupby.split(',')
    df = kfe.extract_keystroke_feature_hash(load_data(args.input, index_cols), index_cols, args.num_features)
    
    if args.normalization != 'none':
        df = kfe.normalize(df, args.normalization, args.norm_distance)
    
    df.to_csv(args.output)
    return